## Brief summary of the problem:
  <TODO: Briefly describe your issue>

## Hardware description:
 - CPU: <TODO>
 - GPU: <TODO>
 - System Memory: <TODO>
 - Display(s): <TODO>
 - Type of Diplay Connection: <TODO: DP, HDMI, DVI, etc>

## System infomration:
 - Distro name and Version: <TODO: e.g., Ubuntu 20.04.1>
 - Kernel version: <TODO: e.g., 5.6.11>
 - Custom kernel: <TODO: e.g., Kernel from drm-misc-next, commit: "Message">
 - AMD package version: <TODO: e.g., "20.02" or "No package">

## How to reproduce the issue:
 < TODO: Describe step-by-step how to reproduce the issue >
 < NOTE: Add as much detail as possible >

## Attached files:
 - Dmesg log
 - Xorg log
 - Any other log
